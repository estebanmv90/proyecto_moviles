<!– PARA EJEMPLO DASC — >

<?php
$id_refaccion_seleccionada = $_GET['refaccion_id'];
$nombre_refaccion_seleccionada = $_GET['refaccion_nombre'];

?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php include'inc/incluye_bootstrap.php' ?>
        <!--termina código que incluye Bootstrap-->
        <?php include 'inc/conexion.php' ?>
    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <form role="form" id="login-form" method="post" class="form-signin" action="refacciones_cotizarproveedor_guardar.php">
                    <div class="h2">
                        Cotizar con el proveedor
                    </div>

                    <div class="form-group">
                        <label>ID refacci&oacute;n (<?php echo $nombre_refaccion_seleccionada ?>)</label>
                        <input type="text" id="id_refaccion" class="form-control" name="id_refaccion" value="<?php echo $id_refaccion_seleccionada ?>" readonly="" 
                               placeholder="<?php echo $nombre_refaccion_seleccionada ?>">
                    </div>

                    <div class="form-group">
                        <label>Selecciona el proveedor con el que est&aacute;s cotizando (requerido)</label><br>
                        <?php
                        //Consulta sin parámetros
                        $sel = $con->prepare("SELECT *from proveedor");
                        $sel->execute();
                        $res = $sel->get_result();
                        $row = mysqli_num_rows($res);
                        ?>
                        <select name="idproveedor">
                                <option value="0">Selecciona el proveedor:</option>
                                <?php while ($f = $res->fetch_assoc()) { ?>
                                    <option value="<?php echo $f['proveedor_id'] ?>"><?php echo $f['proveedor_nombre'] ?></option>        
                                <?php
                                }
                                $sel->close();
                                $con->close();
                                ?>
                        </select>   
                    </div>
                    <div class="form-group">
                        <label>Fecha de solicitud de precio (requerido)</label>
                        <input type="date" class="form-control" id="fecha_solicitud" name="fecha_solicitud" step="1"
                               value="<?php echo date("Y-m-d"); ?>" required>
                    </div>
                    <div class="form-group">
                        <label">Precio $ (requerido)</label>
                        <input type="number" class="form-control" id="precio" name="precio" step="0.01" placeholder="Precio actual" style="text-transform: uppercase;" required>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>
    </body>
</html>