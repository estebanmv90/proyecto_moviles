<?php
include 'inc/conexion.php';
$refaccion_proveedor_id  = "";
$id_refaccion_post = $_POST['id_refaccion'];
$id_proveedor_post = strtoupper($_POST['idproveedor']);
$fecha_solicitud_post = strtoupper($_POST['fecha_solicitud']);
$precio_post = strtoupper($_POST['precio']);

$sel = $con->prepare("SELECT refaccion_proveedor_id,id_refaccion,id_proveedor FROM refaccion_proveedor where id_refaccion=? AND id_proveedor=?");
$sel->bind_param('is', $id_refaccion_post, $id_proveedor_post);
$sel->execute();
$res = $sel->get_result();
$row = mysqli_num_rows($res);

if ($row != 0) {
        header("Location:alerta.php?tipo=fracaso&operacion=YA EXISTE LA REFACCIÓN PARA ESE PROVEEDOR&destino=refacciones_seleccionar.php");

    
} else {
    $ins = $con->prepare("INSERT INTO refaccion_proveedor VALUES(?,?,?,?,?)");
    $ins->bind_param("iiisd", $refaccion_proveedor_id, $id_refaccion_post, $id_proveedor_post, $fecha_solicitud_post, $precio_post);
    if ($ins->execute()) {
        header("Location:alerta.php?tipo=exito&operacion=Refacción cotizada Guardada&destino=refacciones_seleccionar.php");
        
    } else {
        header("Location:alerta.php?tipo=fracaso&operacion=Error al insertar cotizacion Refaccion&destino=refacciones_seleccionar.php");
    }
    $ins->close();
    $con->close();
}
?>
