<?php
include 'inc/conexion.php';
$sucursal_id = "";
$prov_id_post = $_POST['idproveedor'];
$nombre_sucursal_post = strtoupper($_POST['nombre_de_sucursal']);
$direccion_sucursal_post = strtoupper($_POST['direccion_de_sucursal']);
$tel1_sucursal_post = strtoupper($_POST['telefono1_de_sucursal']);
$tel2_sucursal_post = strtoupper($_POST['telefono2_de_sucursal']);
$email_sucursal_post = strtoupper($_POST['correo_electronico']);

$sel = $con->prepare("SELECT sucursal_id,proveedor_id,sucursal_nombre FROM sucursal_prov where proveedor_id=? AND sucursal_nombre=?");
$sel->bind_param('is', $prov_id_post, $nombre_sucursal_post);
$sel->execute();
$res = $sel->get_result();
$row = mysqli_num_rows($res);

if ($row != 0) {
    echo "YA EXISTE LA SUCURSAL " . $nombre_sucursal_post . " PARA EL PROVEEDOR SELECCIONADO";/*
    echo "¿Deseas agregarle un nuevo precio de proveedor?";
    echo "<a href=\"refacciones_proveedores.php?refaccion_id=" . $refaccion_id . "&refaccion_nombre=" . $nombre_refaccion_post . "\" class=\"btn btn-primary\" role=\"button\"> AGREGAR </a>";
    echo "<a href=\"index_refacciones.php\" class=\"btn btn-default\" role=\"button\"> CANCELAR </a>";*/
} else {
    $ins = $con->prepare("INSERT INTO sucursal_prov VALUES(?,?,?,?,?,?,?)");
    $ins->bind_param("iisssss", $sucursal_id, $prov_id_post, $nombre_sucursal_post, $direccion_sucursal_post, $tel1_sucursal_post, $tel2_sucursal_post, $email_sucursal_post);
    if ($ins->execute()) {
        header("Location:alerta.php?tipo=exito&operacion=Sucursal Guardada&destino=sucursal_agregar.php");
    } else {
        header("Location:alerta.php?tipo=fracaso&operacion=Sucursal No Guardada&destino=sucursal_agregar.php");
    }
    $ins->close();
    $con->close();
}
?>
