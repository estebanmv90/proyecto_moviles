<!– PARA EJEMPLO DASC — >
<!DOCTYPE html>
<html>
    <head>
        <title>Seleccionar una Refacci&oacute;n</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>

    </head>
    <body>
        <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <?php
                $sel = $con->prepare("SELECT * from refaccion");
                $sel->execute();
                $res = $sel->get_result();
                ?>
                <div class="h2">
                    Cotizar con el Proveedor
                </div>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <th>ID REFACCIÓN</th>
                    <th>MARCA</th>
                    <th>NOMBRE REFACCION</th>
                    <th>DESCRIPCIÓN</th>
                    <th>IMAGEN</th>
                    </thead>
                    <tfoot>
                    <th>ID REFACCIÓN</th>
                    <th>MARCA</th>
                    <th>NOMBRE REFACCION</th>
                    <th>DESCRIPCIÓN</th>
                    <th>IMAGEN</th>
                    </tfoot>
                    <tbody>
                        <?php while ($f = $res->fetch_assoc()) { ?>
                            <tr>
                                <td>
                                    <?php echo $f['refaccion_id'] ?>
                                </td>
                                <td>
                                    <?php echo $f['marca_id'] ?>
                                </td>
                                 <td>
                                    <?php echo $f['refaccion_nombre'] ?>

                                </td>
                                <td>
                                    <?php echo $f['refaccion_descripcion'] ?>
                                </td>
                                <td>
                                    <a href="refacciones_cotizarproveedor.php?refaccion_id=<?php echo $f['refaccion_id']?>&refaccion_nombre=<?php echo $f['refaccion_nombre'] ?>"> <img class="img-responsive" src="<?php echo $f['refaccion_imagen']?>" /></a>
                                    
                                </td>
                            </tr>
                            <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                    <tbody>
                </table>
            </div>
        </div>
        <?php include'inc/incluye_datatable_pie.php' ?>
    </body>
</html>

